using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour, IDisposable
{
    private References reference;
    private InteractableObjects interactableObjects;
    private CameraController cameraController;
    private InputController inputController;
    private PlayerBase player;
    private DisplayEndGame displayEndGame;

    private void Awake()
    {
        interactableObjects = new InteractableObjects();
        reference = new References();

        displayEndGame = new DisplayEndGame(reference.DisplayEndGame);

        player = reference.Player;

        cameraController = new CameraController(player.transform, reference.MainCamera.transform);
        interactableObjects.AddExecuteObject(cameraController);

        inputController = new InputController(reference.Player);
        interactableObjects.AddExecuteObject(inputController);

        foreach (var obj in interactableObjects)
        {
            if (obj is Obstacle obstacle) 
            {
                obstacle.PlayerKill += KillPlayer;
                obstacle.PlayerKill += displayEndGame.GameOver;
            }
        }

        reference.RestartBtn.onClick.AddListener(RestartGame);
        reference.RestartBtn.gameObject.SetActive(false);
    }

    private void Update()
    {
        for (var i = 0; i < interactableObjects.Length; i++)
        {
            var interactiveObject = interactableObjects[i];

            if (interactiveObject != null)
                interactiveObject.UExecute();
        }
    }

    private void FixedUpdate()
    {
        for (var i = 0; i < interactableObjects.Length; i++)
        {
            var interactiveObject = interactableObjects[i];

            if (interactiveObject != null)
                interactiveObject.FUExecute();
        }
    }

    private void KillPlayer() 
    {
        Destroy(player.gameObject);
        reference.RestartBtn.gameObject.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(sceneBuildIndex: 0);
    }

    public void Dispose()
    {
        foreach (var obj in interactableObjects)
        {
            if (obj is Obstacle obstacle)
            {
                obstacle.PlayerKill -= KillPlayer;
                obstacle.PlayerKill -= displayEndGame.GameOver;
            }
        }
    }
}
