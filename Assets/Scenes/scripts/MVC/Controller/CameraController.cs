using UnityEngine;

public class CameraController : IExecuter
{
    private Transform player;
    private Transform mainCamera;

    public CameraController(Transform player, Transform mainCamera)
    {
        this.player = player;
        this.mainCamera = mainCamera;
    }

    public void FUExecute(){}

    public void UExecute()
    {
        if (player != null && mainCamera != null) 
        {
            var target = new Vector3(player.position.x, player.position.y, mainCamera.position.z);
            var currentPosition = Vector3.Lerp(mainCamera.position, target, 1f * Time.deltaTime);
            mainCamera.position = currentPosition;
        }
        
    }
}
