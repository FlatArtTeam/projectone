using UnityEngine;

public class InputController : IExecuter
{
    private readonly PlayerBase player;

    public InputController(PlayerBase player) 
    {
        this.player = player;
    }

    public void UExecute()
    {
        if (player != null)
            player.Jump(Input.GetKeyDown(KeyCode.Space));
    }

    public void FUExecute()
    {
        if (player != null)
            player.Move(Input.GetAxis("Horizontal"));
    }
}
