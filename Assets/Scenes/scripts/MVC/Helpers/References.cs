using UnityEngine;
using UnityEngine.UI;


public sealed class References
{
    private Camera mainCamera;
    private PlayerHero player;
    private GameObject displayEndGame;
    private Button restartBtn;
    private Canvas canvas;

    public Canvas Canvas
    {
        get
        {
            if (canvas == null)
                canvas = Object.FindObjectOfType<Canvas>();
            return canvas;
        }
    }

    public Button RestartBtn
    {
        get
        {
            if (restartBtn == null)
            {
                var gameObject = Resources.Load<Button>("UI/RestartBtn");
                restartBtn = Object.Instantiate(gameObject, Canvas.transform);
            }
            return restartBtn;
        }

    }

    public GameObject DisplayEndGame 
    {
        get 
        {
            if (displayEndGame == null) 
            {
                var gameObject = Resources.Load<GameObject>("UI/EndGame");
                displayEndGame = Object.Instantiate(gameObject, Canvas.transform);
            }
            return displayEndGame;
        }
        
    }

    public Camera MainCamera
    {
        get
        {
            if (mainCamera == null)
                mainCamera = Camera.main;
            return mainCamera;
        }
    }

    public PlayerHero Player 
    {
        get 
        {
            if (player == null) 
            {
                var gameObject = Resources.Load<PlayerHero>("Player");
                player = Object.Instantiate(gameObject);
            }
            return player;
        }
    }
}
