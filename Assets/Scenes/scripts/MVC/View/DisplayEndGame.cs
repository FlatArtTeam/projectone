using UnityEngine.UI;
using UnityEngine;

public sealed class DisplayEndGame
{
    private Text endGameText;

    public DisplayEndGame(GameObject endGame) 
    {
        endGameText = endGame.GetComponentInChildren<Text>();
        endGameText.text = string.Empty;
    }

    public void GameOver() 
    {
        endGameText.text = $"Game Over";
    }
}
