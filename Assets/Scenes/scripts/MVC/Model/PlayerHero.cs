using UnityEngine;

public class PlayerHero : PlayerBase
{
    private Rigidbody2D rigidbody;
    private SpriteRenderer spriteRenderer;
    public bool isGrounded = false;
    public bool controlEnabled = true;


    void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public override void Move(float x)
    {
        if (controlEnabled) 
        {
            rigidbody.velocity = new Vector2(x * Speed, rigidbody.velocity.y);
            Flip(x);
        }
    }

    public override void Jump(bool jump)
    {
        if (jump && isGrounded)
            rigidbody.AddForce(new Vector2(0, JumpForce));
    }

    private void Flip(float move)
    {
        if (move > 0)
            spriteRenderer.flipX = false;
        else if (move < 0)
            spriteRenderer.flipX = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
            controlEnabled = true;
        }
        if (collision.gameObject.CompareTag("Wall"))
            if (!isGrounded)
                controlEnabled = false;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
            isGrounded = false;
    }
}
