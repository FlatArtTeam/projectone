using UnityEngine;

public abstract class PlayerBase : MonoBehaviour
{
    public float Speed = 3.0f;
    public float JumpForce = 300f;

    public abstract void Move(float x);
    public abstract void Jump(bool jump);
}
