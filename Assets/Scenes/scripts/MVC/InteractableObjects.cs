using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;

public sealed class InteractableObjects : IEnumerator, IEnumerable
{
    private List<IExecuter> interactiveObjects;
    private int _index;
    
    public InteractableObjects() 
    {
        var objects = Object.FindObjectsOfType<InteractiveObject>();
        interactiveObjects = new List<IExecuter>();

        for (var i = 0; i < objects.Length; i++)
            if (objects[i] is IExecuter)
                interactiveObjects.Add(objects[i]);

        _index = -1;
    }

    public int Length => interactiveObjects.Count;

    public object Current => interactiveObjects[_index];

    public void AddExecuteObject(IExecuter execute)
    {
        interactiveObjects.Add(execute);
    }

    public IExecuter this[int index]
    {
        get => interactiveObjects[index];
        private set => interactiveObjects[index] = value;
    }

    public bool MoveNext()
    {
        if (_index == interactiveObjects.Count - 1)
        {
            Reset();
            return false;
        }

        _index++;
        return true;
    }

    public void Reset() => _index = -1;

    public IEnumerator GetEnumerator()
    {
        return this;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}
