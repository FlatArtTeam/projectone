using UnityEngine;

public abstract class InteractiveObject : MonoBehaviour, IExecuter
{
    private bool isInteractable;
    protected bool IsInteractable 
    {
        get => isInteractable;
        private set 
        {
            isInteractable = value;
            GetComponent<Collider2D>().enabled = isInteractable;
        }
    }

    protected abstract void Interaction();
    public abstract void UExecute();
    public abstract void FUExecute();

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!IsInteractable || !collision.CompareTag("Player")) return;
        Interaction();
        IsInteractable = false;
    }

    void Start()
    {
        IsInteractable = true;
    }
}
