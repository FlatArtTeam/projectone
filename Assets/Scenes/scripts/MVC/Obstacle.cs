using System;
using UnityEngine;

public class Obstacle : InteractiveObject
{
    public event Action PlayerKill = delegate () { };
    public override void FUExecute(){}

    public override void UExecute() {}

    protected override void Interaction()
    {
        Debug.Log("Player was kill");
        PlayerKill.Invoke();
    }
}
